import java.util.*;

public class ObjectSorting
{
    public static void main(String[] args)
    {
        int[] intArr = {5, 9 ,1 , 10};
        Arrays.sort(intArr);
        System.out.println(Arrays.toString(intArr));

        String[] strArr = {"A", "C", "B", "Z", "E"};
        Arrays.sort(strArr);
        System.out.println(Arrays.toString(strArr));

        List<String> strList = new ArrayList<>();
        strList.add("A");
        strList.add("C");
        strList.add("B");
        strList.add("Z");
        strList.add("E");
        Collections.sort(strList);
        for(String str : strList)
        {
            System.out.println(str);
        }

        Employee[] employeeArray = new Employee[4];
        employeeArray[0] = new Employee(10, "Jack", 20, "Sales", 20000);
        employeeArray[1] = new Employee(20, "Vilandas", 19, "Crew member", 15000);
        employeeArray[2] = new Employee(5, "Marisa", 21, "Counsellor", 40000);
        employeeArray[3] = new Employee(1, "Aleksandrs", 20, "Cat sitter", 15000);

        Arrays.sort(employeeArray, Employee.SalaryComparator);
        System.out.println(Arrays.toString(employeeArray));

        Arrays.sort(employeeArray, Employee.AgeComparator);
        System.out.println(Arrays.toString(employeeArray));

        Arrays.sort(employeeArray, Employee.NameComparator);
        System.out.println(Arrays.toString(employeeArray));

        Arrays.sort(employeeArray, new EmployeeComparatorByIdAndName());
        System.out.println(Arrays.toString(employeeArray));

    }
}
