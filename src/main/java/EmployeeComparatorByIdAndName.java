import java.util.Comparator;

public class EmployeeComparatorByIdAndName implements Comparator<Employee>
{
    @Override
    public int compare(Employee employeeOne, Employee employeeTwo)
    {
        int flag = employeeOne.getId() - employeeTwo.getId();
        if(flag == 0)
        {
            flag = employeeOne.getName().compareTo(employeeTwo.getName());
        }
        return flag;
    }
}
