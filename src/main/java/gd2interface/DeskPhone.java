package gd2interface;

public class DeskPhone implements ITelephone
{
    private int myNumber;
    private boolean isRinging;

    public DeskPhone(int myNumber)
    {
        this.myNumber = myNumber;
        this.isRinging = false;
    }

    @Override
    public void powerOn()
    {
        System.out.println("No action taken, desktop phones are always one");
    }

    @Override
    public void dial(int phoneNumber)
    {
        System.out.println("Now ringing " + phoneNumber + " on deskphone");
    }

    @Override
    public void answer()
    {
        if(isRinging)
        {
            System.out.println("Answering the desktop phone");
            isRinging = false;
        }
    }

    public void setRinging(boolean ringing)
    {
        isRinging = ringing;
    }

    @Override
    public boolean isRinging()
    {
        return isRinging;
    }
}
