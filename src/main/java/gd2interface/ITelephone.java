package gd2interface;

public interface ITelephone
{
    void powerOn();
    void dial(int phoneNumber);
    void answer();
    boolean isRinging();
}
