package gd2interface;

public class MobilePhone implements ITelephone
{
    private int myNumber;
    private boolean isRinging;
    private boolean isOn = false;

    public MobilePhone(int myNumber)
    {
        this.myNumber = myNumber;
        this.isRinging = false;
    }

    @Override
    public void powerOn()
    {
        this.isOn = true;
        System.out.println("Turning on the phone");
    }

    @Override
    public void dial(int phoneNumber)
    {

    }

    @Override
    public void answer()
    {

    }

    @Override
    public boolean isRinging()
    {
        return false;
    }
}
