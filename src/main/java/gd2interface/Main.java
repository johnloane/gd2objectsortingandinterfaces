package gd2interface;

public class Main
{
    public static void main(String[] args)
    {
        ITelephone adamsPhone;
        adamsPhone = new DeskPhone(1234567);
        adamsPhone.powerOn();
        adamsPhone.dial(456789);
        ((DeskPhone) adamsPhone).setRinging(true);
        adamsPhone.answer();
    }
}
